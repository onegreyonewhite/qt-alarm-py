#!/usr/bin/env python
# =*= coding: utf-8 =*=
from __future__ import unicode_literals

import os
import sys
from PyQt5.QtCore import QTimer, QDateTime
from PyQt5.QtMultimedia import QSound
from PyQt5.QtWidgets import QApplication, QWidget, QSystemTrayIcon
from PyQt5.uic import loadUi


class TrayIcon(QSystemTrayIcon):
    def __init__(self, parent, **kwargs):
        super(TrayIcon, self).__init__()
        self.setToolTip("Будильник" "\n"
                        "Демонстрационная программа-будильник.")
        self.setIcon(parent.windowIcon())

    def _print(self, message):
        self.showMessage("Будильник", message, self.Information, 2000)


class Window(QWidget):
    def __init__(self, *args, **kwargs):
        super(Window, self).__init__(*args, **kwargs)
        self.ui = loadUi("window.ui", self)
        self.ui.stopButton.hide()
        self.alarmTimeEdit.setDateTime(QDateTime.currentDateTime())
        self.moveToCenter()

        self.alarmSound = QSound("smokealarm.wav")
        self.alarmSound.setLoops(10)

        self.tray = TrayIcon(self)
        self.tray.show()

        self.tray.activated.connect(self.onTray)
        self.ui.startButton.clicked.connect(self.start)
        self.ui.stopButton.clicked.connect(self.stop)

        self.timer = QTimer(self)
        self.timer.timeout.connect(self.updateDateTime)

    def onTray(self, reason):
        if(not self.isVisible()):
            self.show()
            self.moveToCenter()
        else:
            self.hide()

    def closeEvent(self, event):
        if(self.timer.isActive()):
            self.stop()
        self.tray.hide()
        event.accept()

    def start(self):
        self.timer.start(5000)
        self.tray._print("Будильник взведён!")
        self.hide()
        self.ui.startButton.hide()
        self.ui.stopButton.show()
        self.ui.alarmTimeEdit.setReadOnly(True)
        print("Таймер запущен.")

    def stop(self):
        self.tray._print("Будильник остановлен!")
        self.ui.alarmTimeEdit.setDateTime(QDateTime.currentDateTime())
        self.ui.startButton.show()
        self.ui.stopButton.hide()
        self.ui.alarmTimeEdit.setReadOnly(False)
        self.timer.stop()
        self.alarmSound.stop()
        self.showNormal()
        self.resize(430, 179)
        self.moveToCenter()
        print("Таймер остановлен.")

    def updateDateTime(self):
        currentDT = QDateTime.currentDateTime()
        targetDT = self.ui.alarmTimeEdit.dateTime()

        if (currentDT >= targetDT):
            self.timer.setInterval(2000)
            self.tray._print("Сработал будильник!!!")

            if (not self.isVisible()):
                self.showMaximized()
                self.alarmSound.play()
                self.ui.stopButton.setFocus()
                self.ui.stopButton.setDefault(True)
                self.ui.stopButton.setAutoDefault(True)

        if (self.ui.stopButton.isVisible()):
            msg = "Текущее время: {} Время срабатывания: {}"
            print(msg.format(currentDT.toString(), targetDT.toString()))


    def moveToCenter(self):
        dCenter = qApp.desktop().availableGeometry(self).center()
        self.move(dCenter - self.rect().center());


def main():
    os.chdir(os.path.dirname(__file__))
    
    global qApp
    qApp = QApplication(sys.argv)
    qApp.setApplicationName("Будильник")

    w = Window()
    w.show()

    sys.exit(qApp.exec_())


if __name__ == '__main__':
    main()
